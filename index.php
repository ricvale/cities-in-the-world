<!Doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		  
		 <!-- Latest compiled and minified CSS -->
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	    <!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<style>
			body{ font-family: Helvetica, Arial; }
			.title 
			{
				margin-top:70px;
				padding:20px;
				border: 1px solid black;
				color: black;
				font-size: 24px;
				text-align: center;
				font-weight:600;
				background-color:#f8e71c;
			}
			.subline
			{
				margin:37px 0 32px 0; 
				color:slategray;
				text-align: center;
			}
			.nccode
			{
				font-size: 17px;
			}
			.canon
			{
				font-size: 11px;
			}
			city div
			{
				border-bottom: 3px solid gainsboro;
				padding:20px 0px;
				height:80px;
			}
			.pagination{ text-align: center; margin-top:50px; }
			.butpag {color: white; background: #bd10e0; border:1px solid gray; }
			.butpag:hover {color: white; background: #bd10e0; border:1px solid black; }
			
			@media only screen and (min-width :1350px)
			{
				.title { max-width:82%; }
			}
			@media only screen and (max-width :767px)
			{
				.pagination { width:100%; text-align: center; }
			}
		</style>
	</head>
	<body>
	
	<div class="container title">Cities in the World</div>
	<div class="container subline">Below is a list of all the top cities in the world. Feel free to browse.</div>
	
	<div class="container" id="content"></div>
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	<script>
	var nrec = 15 ; //numbers of record to display
	var last_line = 86461 ; // Put here the number of the last line of file data.csv
	var index = 1; // start at page load ..... 
													/* var index = last_line - nrec + 1 +13; // For purpose of test */

	function get_records(index){
			$.ajax({ 
			type: 'GET', 
			url: 'get_cities.php', 
			data: { pointer: index }, 
			dataType: 'json',
			success: function (data) { 
				$('#content').html(''); // Clear
				$.each(data, function(i, element) {
					/* Template */
					$('#content').append('<div class="col-lg-4"><city>'
					+'<div><span class="nccode">' + element['Name'] + ', ' + element['Country Code'] + '</span>'
					+'<br><span class="canon">' + element['Canonical Name'] + '</span>'
					+ '</div></city></div>');	
					
					
					if(i == (index % nrec)-1 )
					{	
						canonical_array = (element['Canonical Name']).split(",");
						var url_end = "";
						for(j=(canonical_array.length - 1); j >= 0; j--){ url_end += "/" + canonical_array[j] }
						city_start_in_url = element['Country Code'] + url_end ;
					}
				});
				
				// Buttons of Pagination:
				if(index <= 1) { display_prev = "hide" } else { display_prev = ""; }
				if(index >= last_line - nrec +1 ) { display_next = "hide" } else { display_next = ""; } //Over last index content of records
				
				$('#content').append('<div class="container pagination"><input type="button" id="prev" class="btn butpag '+display_prev+'" value="Previous" /> '
				+' <button id="next" class="btn butpag '+display_next+'" type="button">Next</button></div>');
				
				// custom URL for page
				window.history.pushState("", "", '#/page/'+ Math.ceil(index / nrec) + "/start@/" + city_start_in_url );
			}
		});
	}
	
	$(document).ready(function () {	
		get_records(index); // Init at start index
		
		$('#content').on('click', '#next', function() {
				event.preventDefault();
				index = index + nrec; // new index
				get_records(index);	
		});	
		$('#content').on('click', '#prev', function() {
				event.preventDefault();
				index = index - nrec; // new index
				get_records(index);	
		});
	});
	</script>

	</body>
</html>